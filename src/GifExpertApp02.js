import React, { useState } from 'react'
import { AddCategory } from './components/AddCategory'

export const GifExpertApp = () => {

    const [categories, setCategories] = useState(['One punch', 'Samurai X', 'Dragon Ball'])

    /*
    const handleAdd = () => {
        setCategories([...categories, 'newElement']);

        //otra forma
        //setCategories( cats => [...categories, 'newElement']);
    }
    */

    return (
        <>
            <h2>GifExpertApp</h2>
            <AddCategory setCategories={setCategories} categories={categories} ></AddCategory>
            <hr/>

            <ol>
                {
                    categories.map((category, i) => {
                        return <li key={category}>{category}</li>
                    })
                }
            </ol>
        </>
    )
}