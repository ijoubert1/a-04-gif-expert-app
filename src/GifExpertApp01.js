import React, { useState } from 'react'

export const GifExpertApp = () => {

    //const categories = ['One punch', 'Samurai X', 'Dragon Ball'];
    const [categories, setCategories] = useState(['One punch', 'Samurai X', 'Dragon Ball'])

    const handleAdd = () => {
        setCategories([...categories, 'newElement']);

        //otra forma
        //setCategories( cats => [...categories, 'newElement']);
    }

    return (
        <>
            <h2>GifExpertApp</h2>
            <hr/>

            <button onClick={handleAdd}>Agregar</button>
            <ol>
                {
                    categories.map((category, i) => {
                        return <li key={category}>{category}</li>
                    })
                }
            </ol>
        </>
    )
}