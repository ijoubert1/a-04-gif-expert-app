import React, { useState } from 'react'
import { AddCategory } from './components/AddCategory'
import { GifGrid } from './components/GifGrid'

export const GifExpertApp = () => {

    const [categories, setCategories] = useState(['One punch'])


    return (
        <>
            <h2>GifExpertApp</h2>
            <AddCategory setCategories={setCategories} categories={categories} ></AddCategory>
            <hr/>

            <ol>
                {
                    categories.map((category, i) =>
                        //<li key={category}>{category}</li>)
                        <GifGrid key={category} category={category}/>
                    )}
            </ol>
        </>
    )
}