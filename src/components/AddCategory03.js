import React, { useState } from 'react';
import PropTypes from 'prop-types';

export const AddCategory = ({setCategories, categories}) => {

    //Dejando el valor por defecto vacio
    const [inputValue, setInputValue] = useState('');
    
    const handleInputChange = (e) =>{
        //console.log(e.target.value);
        setInputValue(e.target.value);
    }

    const handleSubmit = (e) =>{
        //prevenir que se refresque toda la pag 
        e.preventDefault();

        if(inputValue.trim().length>2){
            setCategories( cats => [...categories, inputValue]);
            setInputValue('');
        }
        //console.log('submit hecho');
        
    }

    return (
       <form onSubmit={handleSubmit}>
            {/*<h2>AddCategory</h2>*/}
            <input 
                type="text" 
                value={inputValue}
                onChange={handleInputChange}/>

       </form>
    )
}

AddCategory.propTypes = {
    setCategories: PropTypes.func.isRequired,
    categories: PropTypes.array,
}
