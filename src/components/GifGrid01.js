import React from 'react'

export const GifGrid = ({category}) => {

    const getGifs = async() => {
        //se probó en postman
        const url ='https://api.giphy.com/v1/gifs/search?q=Rick+and+Morty&limit=10&api_key=MNEZb9HLD1mWtYvJk9foEijIPUTMKjx3';
        const resp = await fetch(url);
        const {data} = await resp.json();

        const gifs = data.map(img =>{
            return {
                id: img.id,
                title: img.title,
                url: img.images?.downsized_medium,
            }
        })
        //console.log(data);
        console.log(gifs);
    }

    getGifs(); 

    return (
        <div>
            <h3>{category}</h3>
        </div>
    )
}
