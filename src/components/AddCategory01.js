import React, { useState } from 'react'

export const AddCategory = () => {

    const [inputValue, setInputValue] = useState('Ingrese texto');
    
    const handleInputChange = (e) =>{
        //console.log(e.target.value);
        setInputValue(e.target.value);
    }

    const handleSubmit = (e) =>{
        //prevenir que se refresque toda la pag 
        e.preventDefault();
        console.log('submit hecho');
    }

    return (
       <form onSubmit={handleSubmit}>
            {/*<h2>AddCategory</h2>*/}
            <input 
                type="text" 
                value={inputValue}
                onChange={handleInputChange}/>

       </form>
    )
}
