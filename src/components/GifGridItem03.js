import React from 'react'

export const GifGridItem = ({title, url}) => {
    //console.log(props);
    
    return (
        <div className='card'>
            <img src={url.url} alt={title}></img>
            <p>{title}</p>
        </div>
    )
}
