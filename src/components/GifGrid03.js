import React, { useEffect, useState } from 'react'

export const GifGrid = ({ category }) => {

    const [images, setImages] = useState([]);

    useEffect(() => {
        getGifs();
    }, []);


    const getGifs = async () => {

        const url = 'https://api.giphy.com/v1/gifs/search?q=Rick+and+Morty&limit=10&api_key=MNEZb9HLD1mWtYvJk9foEijIPUTMKjx3';
        const resp = await fetch(url);
        const { data } = await resp.json();

        const gifs = data.map(img => {
            return {
                id: img.id,
                title: img.title,
                url: img.images?.downsized_medium,
            }
        })

        console.log(gifs);
        setImages(gifs);
    }

    return (
        <div>
            <h3>{category}</h3>
            <ol>
                {
                    /*
                    images.map(img => {
                        return <li key={img.id}>{img.title}</li>
                    })
                    */

                    //otra forma
                    images.map(({id, title}) => {
                        return <li key={id}>{title}</li>
                    })
                }
            </ol>
        </div>
    )
}
