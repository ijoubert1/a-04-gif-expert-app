import React from 'react'
import { useFetchGifs } from '../hooks/useFetchGifs';
// import { getGifs } from '../helpers/getGifs';
import { GifGridItem } from './GifGridItem';

export const GifGrid = ({ category }) => {
    /*
    const state = useFetchGifs();
    console.log(state);
    */

    //otra forma
    //const {data, loading}= useFetchGifs();
    //console.log(data);

    const {loading}= useFetchGifs();
    console.log(loading);

    // const [images, setImages] = useState([]);

    // useEffect(() => {
    //     //getGifs(category).then(imgs => setImages(imgs));
        
    //     //otra forma
    //     getGifs(category).then(setImages);
    // }, [category]);

    return (
        <>
            <h3>{category}</h3>
            {loading ? 'Cargando...' : 'Data cargada'}

            {/* <div className='card-grid'>
                
                {
                    images.map((img) => 
                    

                    //otra forma
                    <GifGridItem key={img.id} {...img}/>
                    )
                }
            </div> */}
        </>
    )
}
