import React, { useState } from 'react';

export const AddCategory = ({setCategories, categories}) => {

    const [inputValue, setInputValue] = useState('Ingrese texto');
    
    const handleInputChange = (e) =>{
        //console.log(e.target.value);
        setInputValue(e.target.value);
    }

    const handleSubmit = (e) =>{
        //prevenir que se refresque toda la pag 
        e.preventDefault();

        if(inputValue.trim().length>2){
            setCategories( cats => [...categories, inputValue]);
            setInputValue('');
        }
        //console.log('submit hecho');
        
    }

    return (
       <form onSubmit={handleSubmit}>
            {/*<h2>AddCategory</h2>*/}
            <input 
                type="text" 
                value={inputValue}
                onChange={handleInputChange}/>

       </form>
    )
}